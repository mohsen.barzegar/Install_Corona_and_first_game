# *In the name of GOD*


## INSTALL :
***
1.	Download Corona SDK from here : <https://coronalabs.com> (it's about 62.3 MB)
	(you should make a account in site)

2.	Install Corona sdk
	(`How to install corona` video is in `installation` folder)

3.	Now you need a editor, I uesed [Atom](https://atom.io) ,it's available on it's site & `installation` 		folder
	There is a add-on package for Atom editor for corona auto-complete
	*	For install `autocomplete-corona` package on Atom :
	File -> Settings -> install -> insert `autocomplete-corona` in `Search packages` field
	
4.	You need `language-lua` package, if it's not install on your Atom, install it like previos step
	(you need this package becuase, we will program with it)
	
5.	For build .apk file you need `JDK-x86`, if you don't have it, you can download it
	from <http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html>
	or `installation` folder
	and install.
	
	
## First Project :
***
1.	Open `Corona Simulator`
2.	Click on `New Project`
3.	Insert your app's name and select your project location
	*	(Project Template = `Blank` let the another options be defualt )
4.	Three photos (`background.png`, `platform.png` and `ballon.png`) added to projocet folder
5.	Now we will open `main.Lua` file in project folder with Atom
6.	And add following codes :

```lua
local background = display.newImageRect( "background.png", 360, 570 )
background.x = display.contentCenterX
background.y = display.contentCenterY

local platform = display.newImageRect( "platform.png", 300, 50 )
platform.x = display.contentCenterX
platform.y = display.contentHeight-25

local balloon = display.newImageRect( "balloon.png", 112, 112 )
balloon.x = display.contentCenterX
balloon.y = display.contentCenterY
balloon.alpha = 0.8

local physics = require( "physics" )
physics.start()

physics.addBody( platform, "static" )

physics.addBody( balloon, "dynamic", { radius=50, bounce=0.3 } )

local function pushBalloon()
    balloon:applyLinearImpulse( 0, -0.75, balloon.x, balloon.y )
end

balloon:addEventListener( "tap", pushBalloon )
```

7.	Now game is ready, you can go to `Corona Simulator` and play game
8.	For build a `.apk` file, go to :
	File -> Build -> Android
9.	Select your Apk file name and select folder to save and click on `Build`


## usefull links :
1. [Learning lua](https://www.youtube.com/playlist?list=PLxgtJR7f0RBKGid7F2dfv7qc-xWwSee2O)